import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../Home';
import Details from '../Details'
import Constants from '../Constants'
const Stack = createStackNavigator();

export function StackNavigation() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerBackAccessibilityLabel: '',
            }}
            initialRouteName={Constants.AppPaths.HOME}>
            <Stack.Screen name={Constants.AppPaths.HOME} component={Home} />
            <Stack.Screen name={Constants.AppPaths.DETAILS} component={Details} />
        </Stack.Navigator>
    );
}