import React from 'react';
import { Text, View, Image } from 'react-native';
import styles from './styles'
const Details = (props) => {
    let listItemData = props.route.params.item;
    return (
        <View style={styles.containerView}>
            <Text style={styles.textView}>Track Name :</Text>
            <Text style={styles.text}>{listItemData.trackName}</Text>
            <Text style={styles.textView}>Collection Name :</Text>
            <Text style={styles.text}>{listItemData.collectionName}</Text>
            <Text style={styles.textView}>Artist Name :</Text>
            <Text style={styles.text}>{listItemData.artistName}</Text>
            <Text style={styles.textView}>Collection Censored Name :</Text>
            <Text style={styles.text}>{listItemData.collectionCensoredName}</Text>
            <Text style={styles.textView}>Track Censored Name :</Text>
            <Text style={styles.text}>{listItemData.trackCensoredName}</Text>
        </View>
    )
}
export default Details;