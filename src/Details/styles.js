import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    containerView: {
        flex: 1,
        margin: 10
    },
    textView: {
        padding: 10,
        fontWeight: 'bold'
    },
    text: {
        padding: 10,
        marginRight: 2,
        alignContent: 'stretch',
    },
});

export default styles;
