import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    flatList: {
        marginHorizontal: 1,
    },
    divider: {
        paddingVertical: 1,
    }
});

export default styles;
