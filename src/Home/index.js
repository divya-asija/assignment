import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, Image } from 'react-native';
import axios from 'axios';
import Loader from '../Loader';
import styles from './styles';
import ListItem from '../ListItem';

const Home = () => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);


    loadAPI = () => {
        setLoading(true);
        axios.get('https://itunes.apple.com/search?term=Michael+jackson')
            .then(res => {
                setList(res.data.results)
                setLoading(false)
            })
    };

    useEffect(() => {
        loadAPI()
    }, [])
    return (
        <View>
            {loading && <Loader />}
            <FlatList
                data={list}
                style={styles.flatList}
                extraData={list}
                ItemSeparatorComponent={() => <View style={styles.divider} />}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => <ListItem {...item} />}
                keyExtractor={(item) => item.trackId.toString()}
            />

        </View>
    )
}
export default Home;