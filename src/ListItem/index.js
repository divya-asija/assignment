import React from 'react';
import { Text, Image, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';
import Constants from '../Constants';

const ListItem = (props) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity style={[{ flex: 1 }, styles.listItem]} onPress={() => navigation.navigate(Constants.AppPaths.DETAILS, { item: props })}>
            <Image source={{ uri: 'https://picsum.photos/200' }} style={styles.imageViewContainer} />
            <View style={styles.textView}>
                <Text style={styles.textViewHeader} >{props.trackName}</Text>
                <Text>{props.artistName}</Text>
            </View>
        </TouchableOpacity>
    )
}
export default ListItem;