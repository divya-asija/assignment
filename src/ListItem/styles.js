import { StyleSheet, Platform } from 'react-native';

export const styles = StyleSheet.create({
    listItem: {
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 12,
        borderRadius: 2,
        backgroundColor: '#87ceeb',
        flexDirection: 'row'
    },
    imageViewContainer: {
        width: '30%',
        height: 100,
        margin: 10,
        borderRadius: 10
    },
    textView: {
        flex: 1,
        textAlignVertical: 'center',
        alignItems: 'center'
    },
    textViewHeader: {
        fontWeight: 'bold'
    },
});

export default styles;
