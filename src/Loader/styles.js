import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view: {
        position: 'absolute',
        zIndex: 10,
        opacity: 0.9,
        flex: 1,
        height: 100,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    indicator: {
        marginTop: 45
    }
});
