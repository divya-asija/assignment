import React from 'react';
import { Text, View, ActivityIndicator, Modal } from 'react-native';
import styles from './styles';

const Loader = () => {
    return (
        <View style={{ ...styles.view }}>
            <Modal animationType='fade' transparent={true} >
                <ActivityIndicator color={'gray'} size={12} style={styles.indicator} />
            </Modal>
        </View>
    )
}
export default Loader;