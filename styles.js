import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: '#707070'
    }
});

export default styles;

