import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { SafeAreaView } from 'react-native';
import { StackNavigation } from './src/Navigation'
import styles from './styles';

const App = () => {
  return (
    <>
      <NavigationContainer >
        <SafeAreaView style={styles.safeAreaView}>
          <StackNavigation />
        </SafeAreaView>
      </NavigationContainer>
    </>
  );
};

export default App;